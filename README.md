# Sprint1: from 2016-08-29

  - (fixed)Bug: the selected channel will go back to "default" one after saving a learning item
  - (should be fixed by add a preference loadUrlTimeoutValue)Bug: can't enter App without WIFI.
  - (fixed by testing if tag[0] is really need to flesh)Bug: reload from local backup endwith no tags
  - (Added)New: add QR code of weichat account in main page
  - (fixed)New: make the "Review" button a little bit bigger, so that never misses.
  - (Move to Sprint3)New: Research what waltson can do for this App
  - (done)Update: (主页)更换有频道之后的教学截图.
  - Publish: publish Android app at 小米,应用宝
  - (Done) FollowUp: Enable Weichat account's pocket function
  - FollowUp:Create 百度百科 for 难忘英语
  - FollowUp: Check search engines for EnglishEveryday and 难忘英语 keywords
  - FollowUp: Check Auth0's lock's new version, where Chinese login page is available
  - FollowUp: Check if Auth0's lock's rememberlastlogin available
  - (Move to Sprint2)New: Add a time spinner for backup functions.
  - (Move to Sprint2)Bug: Expand sentence input if needed
  - (Move to Sprint2)New: Whole sentence translation with sentence. but don't insert translation to input.(wrap translation in parenthesis and ignore them when input)
  - (bad Idea,因为word必须绑定learning item一起返回)New: Maybe can optimizing random words function by putting them in ramdata as a tree, and load from the tree.
  - (fixed)Bug：新增标签将被添加到。。没有 parent。name
  - (fixed)Bug：移动模式下，删除搜索关键词，会出现没有内容
  - (fixed)Bug：移动到界面没有中文
  - (invalid, check again)Bug：完成移动后没有办法drill down
  - (should't happen)Bug：推荐内容更新后，因为标题变了，所以无法提取出内容
  - (done)New: Enable the ability to check previous advice learning items.
  - (fixed)Bug: Expand sentence input if needed
  - (done)New: Whole sentence translation with sentence. but don't insert translation to input.(wrap translation in parenthesis and ignore them when input)
  - (done)New: Add a time spinner for backup functions.
  
# Spring2 :from 2016-09-04
  - (done)New: 为需要翻译的部分添加了下划线, 使用了减肥版本的marked.js
  - (done)New: 添加打开推送英语翻译提示和完整翻译的功能， 这样用户不用一上来直接看到中文
  - (done for WeiXin browser)Bug: 长按不能识别二维码
  - (done)optimization: 为储存到手机添加一秒的延迟，确保不卡屏幕
  - (done for Android,Do Ios in Mac later)New: Localize App name
  - (done)New：为推荐句子尝试添加text-to-speech
  - (done)New: 使用alert 翻译单词，并在其中添加了发音。
  - Try: using crosswalk for android
  
# Spring3 :from 2016-09-11

  - update: update to Ionic beta 12
  - New: Research what waltson can do for this App， 学了多少生词， 掌握了多少单词量。
  - New: Track how may people is using.