import {Platform, MenuController, Nav, ionicBootstrap, App, AlertController, Storage, LocalStorage} from 'ionic-angular';
import {ViewChild, Component, provide} from '@angular/core';
import {StatusBar, Splashscreen} from 'ionic-native';
import {TabsPage} from './pages/tabs/tabs';
import {AboutPage} from './pages/about/about';
import {DataPage} from './pages/data/data';
import {CloudService} from './providers/cloud.service';          // this will be used as cload upload and download 
import {RamDataService} from './providers/ramdata.service';     // this will be used as ram database
import {LearningItemService} from './providers/learningItem.service';        // this is the sqlite database
import {TagPersistentService} from './providers/tagpersistent.service';
import {AdviceItemService} from './providers/adviceitem.service';
import {IPCService} from './providers/ipc.service';
import {ProfilePage} from './pages/profile/profile';
import {NetworkService} from './providers/network.service';
import {AuthHttp, AuthConfig} from 'angular2-jwt';
import {AuthService} from './providers/auth.service';
import {UpgradeService} from './providers/upgrade.service';
import {Http} from '@angular/http'
import {TutorialPage} from './pages/tutorial/tutorial'
import {TranslateService, TranslatePipe, TranslateLoader, TranslateStaticLoader} from 'ng2-translate/ng2-translate';
import {InAppBrowser} from 'ionic-native';
import * as PouchDB from 'pouchdb';
declare var PouchDB: any;
declare var cordova;

interface PageObj {
    name: string,
    component: any;
    icon: string;
    index?: number;
}

@Component({
    templateUrl: 'build/app.html',
    pipes: [TranslatePipe]
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;
    rootPage: any;
    private eventString;
    private currentPage;
    private isInChinese;
    local: Storage = new Storage(LocalStorage);

    appPages: PageObj[] = [
        { name: 'home', component: TabsPage, icon: 'star' },
        { name: 'review', component: TabsPage, index: 1, icon: 'book' },
        { name: 'input', component: TabsPage, index: 2, icon: 'create' },
        { name: 'search', component: TabsPage, index: 3, icon: 'search' },
    ];
    loggedInPages: PageObj[] = [
        { name: 'profile', component: ProfilePage, icon: 'person' },
        { name: 'about', component: AboutPage, icon: 'information-circle' },
        { name: 'backup', component: DataPage, icon: 'cube' }
    ];

    constructor(
        private app: App,
        private menu: MenuController,
        private platform: Platform,
        private auth: AuthService,
        private ramdata: RamDataService,
        private ipc: IPCService,
        private translate: TranslateService,
        private learningItemPersistent: LearningItemService,
        private tagPersistent: TagPersistentService,
        private alertController: AlertController,
        private upgradeService: UpgradeService
    ) {
        platform.ready().then(() => {
            StatusBar.styleDefault();
            Splashscreen.hide();
            this.registerBackButtonListener();
            return this.loadLocalStorageLanguage();
        });
        this.translateConfig();
        if (platform.is("ios")) {
            learningItemPersistent.initLeanringItemPersistentDatabase(new PouchDB("learningItem", { adapter: 'websql' }));
            tagPersistent.initTagPersistentDatabase(new PouchDB("tags", { adapter: 'websql' }));
        } else {
            learningItemPersistent.initLeanringItemPersistentDatabase(new PouchDB("learningItem"));
            tagPersistent.initTagPersistentDatabase(new PouchDB("tags"));
        }
        // this.ramdata.isDataBaseReady()
        // .then(result =>{
        //     if(result){
        // Must initializeAllDataBase first, then data.
        this.ramdata.forceRefreshData()
        .then(() => {
            let learningItems = this.ramdata.getAllLearningItems();
            this.rootPage = learningItems.length > 0 ? TabsPage : TutorialPage;
        }).then(() => {
            var platfromType;
            if (platform.is('ios')) {
                platfromType = "ios";
            } else if (platform.is('android')) {
                platfromType = "android";
            }
            if (platfromType) {
                this.checkUpgrade(platfromType);
            }
        })
        .then(() =>{
            let learningItems = this.ramdata.getAllLearningItems();
            this.rootPage = learningItems.length > 0 ? TabsPage :  TutorialPage; 
        });
    }
    public checkipcEvent() { // Self-made event system
        this.eventString = this.ipc.getEvent();
        switch (this.eventString) {
            case 'user:login':
                if (this.nav.canGoBack()) {
                    this.nav.pop();
                }
                break;
            case 'swith:toinput':
                this.openPage(this.appPages[2]);
                break;
            default:
                break;
        }
        this.ipc.clearEvent()
        return true;
    }
    private checkUpgrade(platfromType) {
        var that = this;
        this.upgradeService.uploadToCloud(platfromType).subscribe(
            data => {
                if (data.hasNewVersion) {
                    let isChinese = that.ipc.getIsChinese();
                    let options: any = {
                        title: isChinese ? '有新版本' : 'New Version',
                        message: isChinese ? data.message_cn : data.message_en
                    };
                    if (that.platform.is('ios')) {
                        options.buttons = [{
                            text: isChinese ? '更新' : 'Upgrade',
                            handler: () => {
                                that.platform.ready().then(() => {
                                    new InAppBrowser(that.upgradeService.getAppStoreDownloadLink(), "_system", "location=true");
                                });
                            }
                        }];
                    } else if (that.platform.is('android')) {
                        options.buttons = [{
                            text: 'Google Play',
                            handler: () => {
                                that.platform.ready().then(() => {
                                    new InAppBrowser(that.upgradeService.getGooglePlayDownloadLink(), "_system", "location=true");
                                });
                            }
                        }, {
                                text: isChinese ? '主页' : 'Home Page',
                                handler: () => {
                                    that.platform.ready().then(() => {
                                        new InAppBrowser(that.upgradeService.getHomePageDownloadLink(), "_system", "location=true");
                                    });
                                }
                            }];
                    }
                    let confirm = that.alertController.create(options);
                    confirm.present();
                }
            },
            err => {
           	    // return err;
            },
            () => { }
        );
    }
    private registerBackButtonListener() {
        let timePoints = [];
        document.addEventListener('backbutton', () => {
            if (this.nav.canGoBack()) {
                this.nav.pop();
            } else {
                this.confirmExitApp();
            }
        });
    }
    private confirmExitApp() {
        let isChinese = this.ipc.getIsChinese();
        let confirm = this.alertController.create({
            title: isChinese ? '退出APP' : 'Confirm To Exit',
            message: isChinese ? '确定退出？' : 'Really exit app?',
            buttons: [
                {
                    text: isChinese ? '取消' : 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: isChinese ? '确认' : 'Exit',
                    handler: () => {
                        navigator.app.exitApp();
                    }
                }
            ]
        });
        confirm.present();
    }
    public openPage(page: PageObj) {
        this.currentPage = page;
        if (page.index) {
            // page.component is TabsPage for main tabs, by passing the tabIndex, let tabs.ts to decide which page to open.
            this.nav.setRoot(page.component, { tabIndex: page.index });
        } else {
            this.nav.push(page.component);
        }
    }
    private translateConfig() {
        var userLang = navigator.language.split('-')[0]; // use navigator lang if available
        userLang = /(cn|en)/gi.test(userLang) ? userLang : 'cn';
        this.isInChinese = userLang === 'cn';
        this.ipc.setIsChinese(this.isInChinese);
        // this language will be used as a fallback when a translation isn't found in the current language
        this.translate.setDefaultLang('en');

        // the lang to use, if the lang isn't available, it will use the current loader to get them
        this.translate.use(userLang);
    }
    private loadLocalStorageLanguage() {
        let that = this;
        return new Promise(function (fulfill) {
            that.local.get("defaultLanguage").then((defaultLanguage) => {
                var userLang
                if (defaultLanguage) {
                    userLang = defaultLanguage;
                    that.isInChinese = userLang === 'cn';
                    that.ipc.setIsChinese(that.isInChinese);
                    that.translate.use(userLang);
                    fulfill();
                }
            });
        });
    }
    public changeLanguage() {
        let userLang = this.isInChinese ? "cn" : 'en';
        this.local.set("defaultLanguage", userLang);
        this.ipc.setIsChinese(userLang === 'cn');
        this.translate.use(userLang);
    }
}
ionicBootstrap(MyApp, [CloudService, LearningItemService, NetworkService, RamDataService, TagPersistentService, AdviceItemService, UpgradeService, IPCService, provide(AuthHttp, {
    useFactory: (http) => {
        return new AuthHttp(new AuthConfig({ noJwtError: true }), http);
    },
    deps: [Http]
}), AuthService, provide(TranslateLoader, {
    useFactory: (http: Http) => new TranslateStaticLoader(http, 'assets/i18n', '.json'),
    deps: [Http]
}),
    TranslateService], {
        tabbarPlacement: 'bottom'
    });
