import {Component} from '@angular/core';
import {NavController, ToastController, AlertController, LoadingController} from 'ionic-angular';
import {RamDataService} from '../../providers/ramdata.service';
import {TranslatePipe} from "ng2-translate/ng2-translate";
import {IPCService} from '../../providers/ipc.service';
import {CloudService} from '../../providers/cloud.service';
import {AuthService} from '../../providers/auth.service';
import {ProfilePage} from '../profile/profile';

declare var cordova;
declare var window;

@Component({
    templateUrl: 'build/pages/data/data.html',
    pipes: [TranslatePipe]
})
export class DataPage {
    constructor(private ramdata: RamDataService,
        private nav: NavController,
        private ipc: IPCService,
        private cloudService: CloudService,
        private auth: AuthService,
        private alertController: AlertController,
        private toastController: ToastController,
        private loadingCtrl: LoadingController) {
    }
    public saveToFile() {
        let that = this;
        let isChinese = that.ipc.getIsChinese();
        let alert = that.alertController.create({
            title: isChinese ? '备份到手机' : 'Back up to Phone',
            message: isChinese ? '确定要备份文件？ 您将无法找回以前的备份文件' : 'Are you sure? You will lose formal backup files.',
            buttons: [
                {
                    text: isChinese ? '取消' : 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: isChinese ? '确认' : 'Save',
                    handler: () => {
                        that._saveToFile();
                    }
                }
            ]
        });
        alert.present();
    }
    public _saveToFile() {
        let that = this;
        let isChinese = that.ipc.getIsChinese();
        let loading = that.loadingCtrl.create({
            content: isChinese ? '正在写入...' : 'Writing，Please Wait...',
            dismissOnPageChange: true
        });
        loading.present();
        new Promise(function (fulfill, reject) {
            document.addEventListener('deviceready', function () {
                window.resolveLocalFileSystemURL(`${cordova.file.dataDirectory}`, function (dirEntry) {
                    return fulfill(dirEntry);
                }, function (err) {
                    return reject(err);
                });
            }, false);
        }).then(function (dirEntry) {
            return that.createOrGetFile(dirEntry, "backup.json");
        }).then(function (fileEntry) {
            return that.writeFile(fileEntry, {
                learningItems: that.ramdata.allUnfleshedLearningItems(),
                tags: that.ramdata.getAllTagItems()
            });
        }).then(function () {
            let isChinese = that.ipc.getIsChinese();
            let toast = that.toastController.create({
                message: isChinese ? '成功备份到文件' : 'Backup Successful ',
                duration: 1000,
                position: 'top',
                dismissOnPageChange: true
            });
            toast.present();
        }).catch(function (err) {
            let toast = that.toastController.create({
                message: err,
                duration: 1000,
                position: 'top',
                dismissOnPageChange: true
            });
            toast.present();
        }).then(() =>{
            setTimeout(()=>{
                loading.dismiss();
            }, 500);
        });
    }
    private createOrGetFile(dirEntry, fileName) {
        return new Promise(function (fulfill, reject) {
            // Creates a new file or returns the file if it already exists.
            dirEntry.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
                return fulfill(fileEntry);
            }, function (err) {
                return reject(err);
            });
        });
    }
    private writeFile(fileEntry, dataObj) {
        return new Promise(function (fulfill, reject) {
            let that = this;
            // Create a FileWriter object for our FileEntry (log.txt).
            fileEntry.createWriter(function (fileWriter) {
                fileWriter.onwriteend = function () {
                    return fulfill();
                };
                fileWriter.onerror = function (e) {
                    return reject(e.toString());
                };
                // If we are appending data to file, go to the end of the file.
                if (dataObj) {
                    var blob = new Blob([JSON.stringify(dataObj)], { type: 'application/json' });
                    fileWriter.write(blob);
                }
            });
        });
    }
    private readFile(fileEntry) {
        return new Promise(function (fulfill, reject) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function () {
                    return fulfill(JSON.parse(this.result));
                };
                reader.readAsText(file);
            }, function (err) {
                return reject(err);
            });
        });
    }
    public syncFromFile() {
        let that = this;
        let isChinese = that.ipc.getIsChinese();
        let alert = that.alertController.create({
            title: isChinese ? '使用本地备份数据' : 'Sync From Local Backup File',
            message: isChinese ? '确定读取备份文件？ 您将会丢失自从上一次主动备份之后修改但并未备份的内容，新内容不会丢失！！' : 'Are you sure? You will lose all editions since last back up，new inputs shall be fine.',
            buttons: [
                {
                    text: isChinese ? '取消' : 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: isChinese ? '确认' : 'Sync',
                    handler: () => {
                        that._syncFromFile();
                    }
                }
            ]
        });
        alert.present();
    }

    public _syncFromFile() {
        let that = this;
        let isChinese = that.ipc.getIsChinese();
        let loading = that.loadingCtrl.create({
            content: isChinese ? '正在读取...' : 'Reading，Please Wait...',
            dismissOnPageChange: true
        });
        loading.present();
        return new Promise(function (fulfill, reject) {
            document.addEventListener('deviceready', function () {
                window.resolveLocalFileSystemURL(`${cordova.file.dataDirectory}`, function (dirEntry) {
                    return fulfill(dirEntry);
                }, function (err) {
                    return reject(err);
                });
            }, false);
        }).then(function (dirEntry) {
            return that.createOrGetFile(dirEntry, "backup.json");
        }).then(function (fileEntry) {
            return that.readFile(fileEntry);
        }).then(function (result) {
            return new Promise(function (fulfill, reject) {
                if (result) {
                    return fulfill(that.ramdata.syncFromData(result));
                } else {
                    return reject('empty backup');
                }
            });
        }).then(function () {
            return that.ramdata.updateReviewItemsData();
        }).then(function () {
            let isChinese = that.ipc.getIsChinese();
            let toast = that.toastController.create({
                message: isChinese ? '成功读取备份文件' : 'Backup Loaded ',
                duration: 1000,
                position: 'top',
                dismissOnPageChange: true
            });
            toast.present();
        }).catch(function (err) {
            let toast = that.toastController.create({
                message: err,
                duration: 1000,
                position: 'top',
                dismissOnPageChange: true
            });
            toast.present();
        }).then(() =>{
            loading.dismiss();
        });
    }

    public uploadToCloud() {
        let that = this;
        let isChinese = that.ipc.getIsChinese();
        let alert = this.alertController.create({
            title: isChinese ? '备份到云' : 'Back up to Cloud',
            message: isChinese ? '确定要备份文件？ 您将无法找回以前的云备份' : 'Are you sure? You will lose formal cloud backups.',
            buttons: [
                {
                    text: isChinese ? '取消' : 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: isChinese ? '确认' : 'Upload',
                    handler: () => {
                        that._uploadToCloud();
                    }
                }
            ]
        });
        alert.present();
    }
    public _uploadToCloud() {
        let that = this;
        if (!this.auth.authenticated()) {
            this.nav.push(ProfilePage);
            this.auth.login();
        } else {
            let userinfo = this.auth.getUserId();
            var username = (userinfo as any).email;
            let allLearningItems = that.ramdata.allUnfleshedLearningItems();
            let alltags = that.ramdata.getAllTagItems();
            if (allLearningItems.length > 0) {
                let isChinese = that.ipc.getIsChinese();
                let loading = that.loadingCtrl.create({
                    content: isChinese ? '正在上传...' : 'Uploading，Please Wait...',
                    dismissOnPageChange: true
                });
                loading.present();
                let data = {
                    learningItems: allLearningItems,
                    tags: alltags
                }
                that.cloudService.uploadToCloud(username, { 'data': data }).subscribe(
                    data => {
                        let output;
                        let isChinese = that.ipc.getIsChinese();
                        if (isChinese) {
                            output = data === 200 ? "成功上传" : "上传失败";
                        } else {
                            output = data === 200 ? "Upload Success" : "Upload Fail";
                        }
                        let toast = that.toastController.create({
                            message: output,
                            duration: 1000,
                            position: 'top',
                            dismissOnPageChange: true
                        });
                        toast.present();
                    },
                    err => {
                        //                    	console.log(err)
                    },
                    () => { 
                        loading.dismiss();
                    }
                );
            } else {
                let isChinese = that.ipc.getIsChinese();
                let toast = that.toastController.create({
                    message: isChinese ? '没有任何内容可以上传' : 'Nothing to upload',
                    duration: 1000,
                    position: 'top',
                    dismissOnPageChange: true
                });
                toast.present();
            }
        }
    }
    public syncFromCloud() {
        let that = this;
        let isChinese = that.ipc.getIsChinese();
        let alert = this.alertController.create({
            title: isChinese ? '从云同步' : 'Sync From Cloud',
            message: isChinese ? '确定通过云文件同步？您将会丢失修改但并未上传的内容，新内容不会丢失' : 'Are you sure? You will lose all editions since last back up, new inputs shall be fine.',
            buttons: [
                {
                    text: isChinese ? '取消' : 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: isChinese ? '确认' : 'Sync',
                    handler: () => {
                        that._syncFromCloud();
                    }
                }
            ]
        });
        alert.present();
    }
    public _syncFromCloud() {
        let that = this;
        if (!this.auth.authenticated()) {
            this.nav.push(ProfilePage);
            this.auth.login();
        } else {
            let isChinese = that.ipc.getIsChinese();
            let loading = that.loadingCtrl.create({
                content: isChinese ? '正在下载...' : 'Downloading, Please Wait...',
                dismissOnPageChange: true
            });
            loading.present();
            let userinfo = that.auth.getUserId();
            var username = (userinfo as any).email;
            that.cloudService.downloadFromCloud(username).subscribe(
                data => {
                    that._syncFromCloudData(data)
                        .then(() => {
                            loading.dismiss();
                        });
                },
                err => {
                    //                	console.log(err)
                },
                () => { }
            );
        }
    }
    public _syncFromCloudData(data) {
        let that = this;
        if (data) {
            return that.ramdata.syncFromData(data)
                // Create s spinner maybe.
                .then(function () {
                    return that.ramdata.updateReviewItemsData();
                }).then(function () {
                    let isChinese = that.ipc.getIsChinese();
                    let toast = that.toastController.create({
                        message: isChinese ? '成功下载云文件' : 'Sync From Cloud',
                        duration: 1000,
                        position: 'top',
                        dismissOnPageChange: true
                    });
                    toast.present();
                }).catch(function (err) {
                    let toast = that.toastController.create({
                        message: err,
                        duration: 1000,
                        position: 'top',
                        dismissOnPageChange: true
                    });
                    toast.present();
                });
        } else {
            let toast = that.toastController.create({
                message: 'Nothind downloaded',
                duration: 1000,
                position: 'top',
                dismissOnPageChange: true
            });
            toast.present();
            return Promise.resolve();
        }
    }
}
