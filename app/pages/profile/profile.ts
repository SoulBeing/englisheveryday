import { NavController} from 'ionic-angular';
import { Component } from '@angular/core';
import {AuthService} from '../../providers/auth.service';
import {TranslatePipe} from "ng2-translate/ng2-translate";



@Component({
    templateUrl: 'build/pages/profile/profile.html',
    pipes: [TranslatePipe]
})
export class ProfilePage {
  
  // We need to inject AuthService so that we can
  // use it in the view
  constructor(private auth: AuthService) {}
}