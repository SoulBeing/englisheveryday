import { Component} from '@angular/core';
import {NavController, AlertController, ItemSliding, Content} from 'ionic-angular';
import {RamDataService} from '../../providers/ramdata.service';
import {Input} from '../input/input';
import {TranslatePipe} from "ng2-translate/ng2-translate";
import {IPCService} from '../../providers/ipc.service';
import {TranslateService} from '../../providers/translate.service';


@Component({
  templateUrl: 'build/pages/review/review.html',
  providers: [TranslateService],
  pipes: [TranslatePipe]
})
export class Review {
  public reviewType: string;
  public reviewItems = [];
  public newInputsItems = [];
  public wordReviewItems = [];
  private count;

  public setting = {
    showSentence: true,
    showWords: true,
    showWordgroups: true,
    showtags: true,
    shownotes: false
  };
  constructor(private ramdata: RamDataService,
    private nav: NavController,
    private ipc: IPCService,
    private translateService: TranslateService,
    private alertController: AlertController
    ) {
  }
  private ionViewWillEnter() {
    this.updateReviewItemsData();
    if (this.count === 0 && this.newInputsItems.length > 0) {
      this.reviewType = "new";
    } else if (this.count === 0 && this.newInputsItems.length === 0) {
      this.reviewType = "random";
    } else {
      this.reviewType = "standard";
    }

  }
  public updateReviewItemsData() {
    let result = this.ramdata.updateReviewItemsData();
    this.reviewItems = result.reviewItems
    this.newInputsItems = result.newInputsItems
    this.wordReviewItems = result.wordReviewItems
    this.count = result.count;
  }
  public openReviewModal(reviewItem) {
    let that = this;
    this.translateService.translateWordandPhraseforModal(reviewItem.words.join('\n').concat('\n').concat(reviewItem.wordgroups.join('\n')))
      .then(function (wordsTranslationResults) {
        let content = "";
        let wholeArray = reviewItem.words.concat(reviewItem.wordgroups);
        for (let i = 0; i < wholeArray.length; i++) {
          content = content + wholeArray[i] + " : " + wordsTranslationResults[i].dst + '<br/>';
        }
        let alert = that.alertController.create({
          title: content,
          subTitle: (!!reviewItem.note ? reviewItem.note : ""),
          buttons: ['OK']
        });
        alert.present();
      })
  }

  public openReviewModalForWord(reviewItem) {
    let that = this;

    let wholeArray = reviewItem.words.concat(reviewItem.wordgroups);
    let alert = that.alertController.create({
      title: !!reviewItem.sentence ? reviewItem.sentence : "",
      subTitle: (!!reviewItem.note ? reviewItem.note : ""),
      buttons: ['OK']
    });
    alert.present();
  }

  private doneReview(reviewItem) {
    reviewItem.reviewedTimes += 1;
    reviewItem.lastReviewDate = Date.now();
    this.ramdata.updateLearningItem(reviewItem);
    this.updateReviewItemsData();
  }
  public editLearningItem(learningItem, slidingItem: ItemSliding) {
    slidingItem.close();
    this.ipc.publishEvent('swith:toinput');
    learningItem.isNew = false;
    this.ipc.setTransferObject(learningItem);
  }
  public doRefresh(refresher) {
    this.wordReviewItems = this.ramdata.updataWordsForReview();
    refresher.complete();
  }

  checkDivider(reviewgroup) {
    return reviewgroup.items.length > 0;
  }
  checkSentence(reviewItem) {
    return reviewItem && reviewItem.sentence && this.setting.showSentence;
  }
  checkWords(reviewItem) {
    return reviewItem && reviewItem.words[0] && this.setting.showWords;
  }
  checkWordgroups(reviewItem) {
    return reviewItem && reviewItem.wordgroups[0] && this.setting.showWordgroups;
  }
  checkNotes(reviewItem) {
    return reviewItem && reviewItem.note && this.setting.shownotes;
  }
}
