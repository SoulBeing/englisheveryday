import {NavParams, ViewController, ItemSliding, NavController, List, AlertController, ToastController, ModalController} from 'ionic-angular';
import {RamDataService} from '../../providers/ramdata.service';
import { Component } from '@angular/core';
import {TagsToMoveModal} from '../tagsmovemodal/tagsmovemodal';
import {TranslatePipe} from "ng2-translate/ng2-translate";
import {IPCService} from '../../providers/ipc.service';


interface Tag {
    _id: string,
    name: string,
    parent: string
}

@Component({
    templateUrl: 'build/pages/tags/tags.html',
    pipes: [TranslatePipe]
})
export class Tags {
    public tagsList;  // all child tags in current level under 'currentParentTag'
    public listheader;
    public selectedTags;
    private currentParentTag;
    private rootTag = { _id: "root", name: "" };
    public isnotRootTag: boolean = false;
    private isCanceling = false;
    private overallEditingMode = false;
    private readyToMoveindexbooleanList = [];

    constructor(
        private navParams: NavParams,
        private viewCtrl: ViewController,
        private ramdata: RamDataService,
        private nav: NavController,
        private ipc: IPCService,
        private alertController: AlertController,
        private toastController : ToastController,
        public modalCtrl: ModalController
    ) {
        this.selectedTags = this.navParams.data[0] && this.navParams.data || [];
        this.currentParentTag = this.rootTag;
        this.resetTagListToRoot();
    }

    /**
     * Pass seleted tags back to input page.
     */
    applyTags() {
        this.viewCtrl.dismiss(this.selectedTags);
    }

    /**
     * Go to tag's children.
     */
    drilldownOrSelect(tag) {
        if (this.hasChildren(tag)) {
            this.tagsList = this.ramdata.getAllTagItems().filter(t => t.parent === tag._id);
            this.listheader = tag.name;
            this.currentParentTag = tag;
            this.updateBackArrowState();
        } else {
            this.selectTag(tag);
        }
    }

    /**
     * Used to selected current parent tag if not root tag.
     */
    public selectThisParentTag() {
        if (this.currentParentTag && this.currentParentTag.parent) {
            this.selectTag(this.currentParentTag);
        }
    }

    public selectTag(tag) {
        if (!this.isExistInSelection(tag)) {
            this.selectedTags.push(tag);
        }
    }

    /**
     * Go back to tag's parent.
     */
    public goback() {
        if (this.currentParentTag.parent) {
            this.currentParentTag = this.findTag(this.currentParentTag.parent);
            this.tagsList = this.ramdata.getAllTagItems().filter(t => t.parent === this.currentParentTag._id);
            this.listheader = this.currentParentTag.name;
            this.updateBackArrowState();
        }
    }

    /**
     * Delete a tag from selected tags.
     */
    public deleteSelectedTag(tag) {
        this.selectedTags = this.selectedTags.filter(function (r) { return r.name != tag.name; });
    }

    /**
     * Used to rename a tag.
     */
    public renameTag(tag, slidingItem: ItemSliding) {
        let isChinese = this.ipc.getIsChinese();
        let that = this;
        let promptOption = {
            title: isChinese ? '改标签名' : 'Rename Tag',
            message: (isChinese ? '原名为 ' : "Old name is ") + tag.name,
            inputs: [
                {
                    name: 'title',
                    placeholder: isChinese ? '新名字' : 'New Name'
                },
            ],
            buttons: [
                {
                    text: isChinese ? '取消' : 'Cancel',
                    handler: data => { }
                },
                {
                    text: isChinese ? '储存' : 'Save',
                    handler: data => {
                        if (that.tagsList.filter(t => t.name === data.title).length === 0 && data.title !== "") {
                            tag.name = data.title;
                            this.ramdata.updateTagItem(tag);
                        }
                    }
                }
            ]
        };
        if (tag._id === "source" || tag._id === "theme" || tag._id === "content") {
            promptOption.message = isChinese ? '该父级标签将被用于推荐项目，可改名，但不推荐改变其本质含义' : 'Not recommanded to change its inherent meaning, but you can rename.';
        }
        let prompt = that.alertController.create(promptOption);
        prompt.present();
        slidingItem.close();
    }

    /**
     * Used to remove perminently a tag
     */
    public removeTag(tag, slidingItem: ItemSliding) {
        let isChinese = this.ipc.getIsChinese();
        slidingItem.close();
        if (tag._id === "source" || tag._id === "theme" || tag._id === "content") {
            let toast = this.toastController.create({
                message: isChinese ? '无法删除默认标签，但可改名' : 'Default tags cannot be deleted, but can be renamed',
                duration: 2000,
                position: 'middle',
                dismissOnPageChange: true
            });
            toast.present();
        }else if(this.hasChildren(tag)){
            let toast = this.toastController.create({
                message: isChinese ? '还有子标签，无法删除该父级标签' : 'Parent tag cannot be deleted',
                duration: 2000,
                position: 'middle',
                dismissOnPageChange: true
            });
            toast.present();
        }else{
            let alert = this.alertController.create({
                title: isChinese ? '当真要删除？' : 'Confirm Delete',
                message: isChinese ? '词条中的该标签也没了哦' : 'This item will be deleted entirely',
                buttons: [
                    {
                        text: isChinese ? '取消' : 'Cancel',
                        handler: () => {
                        }
                    },
                    {
                        text: isChinese ? '删除' : 'Delete',
                        handler: () => {
                            this.ramdata.removeTag(tag);
                            this.tagsList = this.tagsList.filter(function (r) { return r._id != tag._id; });
                        }
                    }
                ]
            });
            alert.present();
        }
    }

    public addChildTag(tag, slidingItem: ItemSliding) {
        this._addTag(tag, this.getChildren(tag));
        slidingItem.close();
    }

    public addTag() {
        this._addTag(this.currentParentTag, this.tagsList);
    }

    /**
     * Used to add a tag to current level.
     */
    private _addTag(parent, tagslist) {
        let isChinese = this.ipc.getIsChinese();
        let that = this;
        let prompt = this.alertController.create({
            title: isChinese ? '创建新标签' : 'Create New Tag',
            message: (isChinese ? '新标签将被加入到->' : "The new tag will be inserted under ") + parent.name,
            inputs: [
                {
                    name: 'title',
                    placeholder: isChinese ? '标签名' : 'Tag Name'
                },
            ],
            buttons: [
                {
                    text: isChinese ? '取消' : 'Cancel',
                    handler: data => { }
                },
                {
                    text: isChinese ? '确认' : 'Save',
                    handler: data => {
                        if (tagslist.filter(t => t.name === data.title).length === 0 && data.title) { // This line will do the alise check.
                            let newTag = {
                                _id: Date.now() + "",
                                name: data.title,
                                parent: parent._id
                            };
                            tagslist.unshift(newTag);
                            that.saveTag(newTag);
                        }
                    }
                }
            ]
        });
        prompt.present();
    }

    /**
     * Used to search a tag, "in this mode no drill down is needed" changed to able to drill down even in searching mode.
     */
    public searchTags(event) {
        let isChinese = this.ipc.getIsChinese();
        if (!this.isCanceling) {
            let val = event.target.value;
            // if the value is an empty string don't filter the items
            if (val && val.trim() !== '') {
                this.tagsList = this.ramdata.getAllTagItems().filter(t => t.name.search(new RegExp(val.trim(), "i")) !== -1);
                this.listheader = isChinese ? '搜索结果' : "Search Result";
                // TODO maybe will create a brand new search result view in a hearder tree structure.          
            } else {
                this.cancelSearch();
            }
        }
        this.isCanceling = false;
    }

    /**
     * Used to cancel a search.
     */
    public cancelSearch() {
        this.isCanceling = true;
        this.resetTagListToRoot();
    }

    public toggleoverallEdit() {
        this.overallEditingMode = !this.overallEditingMode;
    }

    public moveto() {
        var that = this;
        let selectedTags = [];
        for (let i = 0; i < this.readyToMoveindexbooleanList.length; i++) {
            if (this.readyToMoveindexbooleanList[i]) {
                selectedTags.push(this.tagsList[i]);
            }
        }
        let modal = that.modalCtrl.create(TagsToMoveModal, selectedTags);
        if (selectedTags.length > 0) {
            modal.present();
        }
        modal.onDidDismiss((data: any[]) => {
            if (data) {
                selectedTags.forEach(t => t.parent = data);
                // Up date disk data and ramdata
                that.overallEditingMode = false;
                that.resetTagListToRoot()
                selectedTags.forEach(t => that.ramdata.updateTagItemTOPersistant(t));
            }
        });
    }

    private saveTag(newTag) {
        this.ramdata.saveTagItem(newTag);
    }

    private hasChildren(tag) {
        return this.ramdata.getAllTagItems().some(t => t.parent === tag._id);
    }

    private getChildren(tag) {
        return this.ramdata.getAllTagItems().filter(t => t.parent === tag._id);
    }

    private isExistInSelection(tag) {
        return this.selectedTags.some(t => t._id === tag._id);
    }

    private findTag(tagId) {
        return this.ramdata.getAllTagItems().find(t => t._id === tagId);
    }

    private updateBackArrowState() {
        this.currentParentTag.parent ?
            this.isnotRootTag = true : this.isnotRootTag = false;
    }

    private resetTagListToRoot() {
        this.tagsList = this.ramdata.getAllTagItems().filter(t => t.parent === 'root');
        this.listheader = this.rootTag.name;
    }

}
