import {NavController, AlertController, ItemSliding, PopoverController, ViewController, Content, NavParams, ModalController} from 'ionic-angular';
import {Component, ViewChild, ElementRef } from '@angular/core';
import {RamDataService} from '../../providers/ramdata.service';
import {Input} from '../input/input';
import {Tags} from '../tags/tags';
import {IPCService} from '../../providers/ipc.service';
import {TranslatePipe} from "ng2-translate/ng2-translate";
import {TranslateService} from '../../providers/translate.service';


@Component({
    templateUrl: 'build/pages/search/search.html',
    providers: [TranslateService],
    pipes: [TranslatePipe]
})
export class Search {
    private learningItems = [];
    private selectedTags = [];
    private searchMode;
    public searchBarPlaceholder;
    public searchValue;
    private isFilteringByTags = false; // Used for a requirement, that able to filter contents based on selected tags instead of among all learning items.
    private learningItemsOfTags;
    private _filterTimeout;
    private lastSearchInput = "";

    public setting = {
        showSentence: true,
        showWords: true,
        showWordgroups: true,
        showtags: true,
        shownotes: false
    };

    constructor(private ramdata: RamDataService,
        private nav: NavController,
        private ipc: IPCService,
        private translateService: TranslateService,
        private popoverController : PopoverController,
        private alertController: AlertController,
        public modalCtrl: ModalController
    ) { }

    public presentPopover(myEvent) {
        let popover = this.popoverController.create(PopoverPage, {
            setting: this.setting
        });
        popover.present({
            ev: myEvent
        });
    }
    private ionViewWillEnter() {
        this.learningItems = this.ramdata.getAllLearningItems();
        this.updateSearchBarPlaceHoder();
    }
    private updateSearchBarPlaceHoder() {
        this.searchBarPlaceholder = this.ipc.getIsChinese() ?
            "在以下范围内查找： ".concat(this.setting.showSentence ? " 句子" : "", this.setting.showWords ? " 生词" : "",
                this.setting.showWordgroups ? " 词组组合" : "", this.setting.shownotes ? " 笔记" : "") :
            "Search by".concat(this.setting.showSentence ? " Sentence" : "", this.setting.showWords ? " Words" : "",
                this.setting.showWordgroups ? " Phrase" : "", this.setting.shownotes ? "Note" : "");
    }
    /**
     * This method will call constantly, not only to check the placebar text, but also do filter search at each tick, aka search while typing
     */
    public checkingNewplaceBar() {
        this.updateSearchBarPlaceHoder();
        this.filterLearningItem();
        return true;
    }
    public openDetailModal(learningItem) {
        let that = this;
        this.translateService.translateWordandPhraseforModal(learningItem.words.join('\n').concat('\n').concat(learningItem.wordgroups.join('\n')))
            .then(function (wordsTranslationResults) {
                let content = "";
                let wholeArray = learningItem.words.concat(learningItem.wordgroups);
                for (let i = 0; i < wholeArray.length; i++) {
                    content = content + wholeArray[i] + " : " + wordsTranslationResults[i].dst + '<br/>';
                }
                let alert = that.alertController.create({
                    title: content,
                    subTitle: (!!learningItem.note ? learningItem.note : ""),
                    buttons: ['OK']
                });
                alert.present();
            })
    }

    public editLearningItem(learningItem, slidingItem: ItemSliding) {
        slidingItem.close();
        this.ipc.publishEvent('swith:toinput');
        learningItem.isNew = false;
        this.ipc.setTransferObject(learningItem);
    }
    public deleteLearningItem(learningItem, slidingItem: ItemSliding) {
        let isChinese = this.ipc.getIsChinese();
        slidingItem.close();
        let alert = this.alertController.create({
            title: isChinese ? '当真要删除？' : 'Confirm Delete',
            message: isChinese ? '这句真的没用了么？' : 'This item will be deleted entirely',
            buttons: [
                {
                    text: isChinese ? '取消' : 'Cancel',
                    handler: () => {
                    }
                },
                {
                    text: isChinese ? '删除' : 'Delete',
                    handler: () => {
                        this.ramdata.deleteLearningItem(learningItem);
                        this.learningItems = this.ramdata.getAllLearningItems();
                    }
                }
            ]
        });
        alert.present();
    }
    public cancelSearch() {
        // Reset to default
        this.learningItems = this.isFilteringByTags ? this.learningItemsOfTags : this.ramdata.getAllLearningItems();
        this.removeHighLight();
    }
    public filterLearningItem = function () {
        //  let val = event.target.value;
        let val = this.searchValue;
        let that = this;
        if (val && val.trim() !== '' && val.trim() !== this.lastSearchInput) {

              
        if (that._filterTimeout) {
            window.clearTimeout(that._filterTimeout);
        }
        that._filterTimeout = window.setTimeout(function () {
//            console.log("filtering- doing")
            that.lastSearchInput = val.trim();
            let filteringSource = that.isFilteringByTags ? that.learningItemsOfTags : that.ramdata.getAllLearningItems();
            that.learningItems = filteringSource.filter((item) => {
                return that.hasContent(item, val.trim());
            });
            that.removeHighLight();
            that.addHighLight(val);
            that._filterTimeout = null;
        }, 400);
            // this.lastSearchInput = val.trim();
            // let filteringSource = this.isFilteringByTags ? this.learningItemsOfTags : this.ramdata.getAllLearningItems();
            // this.learningItems = filteringSource.filter((item) => {
            //     return this.hasContent(item, val.trim());
            // });
            // this.removeHighLight();
            // this.addHighLight(val);
        } else if(this.lastSearchInput && (!val || val.trim() === '')){
            // TODO maybe need to optimizing this part, since this code will be called a lot!
//            console.log("filter->cancaling")
            this.lastSearchInput = "";
            this.cancelSearch();
        }
    }
    private addHighLight(val) {
        let searchableElements = document.querySelectorAll('.searchscope');
        Array.prototype.forEach.call(searchableElements, function (element) {
            element.innerHTML = element.innerHTML.replace(new RegExp(val, "ig"), '<span class="highlight">' + val + '</span>');
        });
    }
    private removeHighLight() {
        let searchableElements = document.querySelectorAll('.searchscope');
        Array.prototype.forEach.call(searchableElements, function (element) {
            element.innerHTML = element.innerHTML.replace(/<span class="highlight">/g, '');
            element.innerHTML = element.innerHTML.replace(/<\/span>/g, '');
        });
    }
    private hasContent(learningItemObject, valueToSearch) {
        let result = false;
        if (this.setting.showSentence && learningItemObject.sentence && learningItemObject.sentence.search(new RegExp(valueToSearch, "i")) !== -1) {
            result = true;
        }
        if (this.setting.showWords && learningItemObject.words && learningItemObject.words.toString().search(new RegExp(valueToSearch, "i")) !== -1) {
            result = true;
        }
        if (this.setting.showWordgroups && learningItemObject.wordgroups && learningItemObject.wordgroups.toString().search(new RegExp(valueToSearch, "i")) !== -1) {
            result = true;
        }
        if (this.setting.shownotes && learningItemObject.note && learningItemObject.note.search(new RegExp(valueToSearch, "i")) !== -1) {
            result = true;
        }
        return result;
    }
    /**
     * Delete a tag from selected tags.
     */
    public deleteSelectedTag(tag) {
        this.selectedTags = this.selectedTags.filter(function (r) { return r.name != tag.name; });
        if (this.selectedTags.length === 0) {
            this.learningItems = this.ramdata.getAllLearningItems();
            this.isFilteringByTags = false;
        }
    }

    public selectTagsFilter() {
        let modal =  this.modalCtrl.create(Tags, this.selectedTags);
        modal.present();

        modal.onDidDismiss((data: any[]) => {
            if (data) {
                this.isFilteringByTags = true;
                this.selectedTags = data;
                this.filterLearningItemsByTags(this.selectedTags);
            } else {
                this.isFilteringByTags = false;
            }
        });
    }
    private filterLearningItemsByTags(selectedTags) {
        this.learningItemsOfTags = this.ramdata.getAllLearningItems().filter((item) => {
            return this.isItemhasAllTagsOf(item, selectedTags);
        });
        this.learningItems = this.learningItemsOfTags;
    }
    private isItemhasAllTagsOf(item, selectedTags) {
        let result = false;
        result = selectedTags.every((tag) => {
            return this.isItemhasTagOf(item, tag);
        })
        return result;
    }
    private isItemhasTagOf(item, tarGettag) {
        let result = false;
        result = item.tags && item.tags.some((ItemsingleTag) => {
            if (ItemsingleTag) {
                while (ItemsingleTag) {
                    if (ItemsingleTag._id === tarGettag._id) {
                        return true;
                    } else {
                        ItemsingleTag = this.getParentTag(ItemsingleTag);
                    }
                }
            }
            return false;
        });
        return result;
    }
    private getParentTag(tag) {
        return this.ramdata.getAllTagItems().find(t => t._id === tag.parent);
    }

    ionViewLoaded() { } // works the same way as ngOnInit, will be called only for once
    ionViewDidEnter() { }  //are hooks that are available before and after the page in question becomes active
    ionViewWillLeave() { }
    ionViewDidLeave() { } // are hooks that are available before and after the page leaves the viewport
    ionViewWillUnload() { }
    ionViewDidUnload() { }  //are hooks that are available before and after the page is removed from the DOM
}

@Component({
    template: `
    <ion-list>
      <ion-list-header>{{ "nested.searchPopupTitle" | translate }}</ion-list-header>
        <ion-item>
            <ion-label>{{ "nested.sentence" | translate }}</ion-label>
            <ion-toggle sentence [(ngModel)]="setting.showSentence"></ion-toggle>
        </ion-item>
        <ion-item>
            <ion-label>{{ "nested.words" | translate }}</ion-label>
            <ion-toggle word [(ngModel)]="setting.showWords"></ion-toggle>
        </ion-item>
        <ion-item>
            <ion-label>{{ "nested.wordGroups" | translate }}</ion-label>
            <ion-toggle wordgroup [(ngModel)]="setting.showWordgroups"></ion-toggle>
        </ion-item>
        <ion-item>
            <ion-label>{{ "nested.tags" | translate }}</ion-label>
            <ion-toggle tags [(ngModel)]="setting.showtags"></ion-toggle>
        </ion-item>
        <ion-item>
            <ion-label>{{ "nested.note" | translate }}</ion-label>
            <ion-toggle note [(ngModel)]="setting.shownotes"></ion-toggle>
        </ion-item>
    </ion-list>
  `,
    pipes: [TranslatePipe]
})
class PopoverPage {
    private setting: any;
    constructor(private navParams: NavParams) { }

    ngOnInit() {
        if (this.navParams.data) {
            this.setting = this.navParams.data.setting;
        }
    }
}