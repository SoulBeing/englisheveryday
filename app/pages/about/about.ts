import {Component} from '@angular/core';
import {RamDataService} from '../../providers/ramdata.service';
import {TranslatePipe} from "ng2-translate/ng2-translate";
import {Platform} from 'ionic-angular';
import {InAppBrowser} from 'ionic-native';

@Component({
    templateUrl: 'build/pages/about/about.html',
    pipes: [TranslatePipe]
})
export class AboutPage {
    constructor(private ramdata: RamDataService,
    private platform:Platform) {
    }

    launch(url) {
      this.platform.ready().then(() => {
          new InAppBrowser(url, "_self", "location=true");
      });
  }
}
