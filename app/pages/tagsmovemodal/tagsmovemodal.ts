import {NavParams, ViewController, ItemSliding, NavController, List} from 'ionic-angular';
import {RamDataService} from '../../providers/ramdata.service';
import { Component } from '@angular/core';
import {TranslatePipe} from "ng2-translate/ng2-translate";

interface Tag {
    _id: string,
    name: string,
    parent: string
}

@Component({
    templateUrl: 'build/pages/tagsmovemodal/tagsmovemodal.html',
    pipes: [TranslatePipe]
})
export class TagsToMoveModal {
    public tagsList= [];  // all child tags in current level under 'currentParentTag'
    private allTagsWithoutReadyToMoveOnes;
    public listheader;
    public needToMoveTags;
    private currentParentTag;
    private rootTag = { _id: "root", name: "" };
    public isnotRootTag: boolean = false;
    private selectedIndexofTagList=[];
    private isSearching = false;
    private isCanceling = false;

    constructor(
        private navParams: NavParams,
        private viewCtrl: ViewController,
        private ramdata: RamDataService,
        private nav: NavController
    ) {
        this.needToMoveTags = this.navParams.data[0] && this.navParams.data || [];
        this.currentParentTag = this.rootTag;
        this.resetTagListToRoot();
    }

    /**
     * Pass seleted tags back to input page.
     */
    confirm() {
        let targetParent = this.selectedIndexofTagList;
        this.viewCtrl.dismiss(targetParent);
    }
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * Go to tag's children.
     */
    drilldownOrSelect(tag) {
        if (this.isSearching) {
            // No drill down is needed when searching       
        } else {
            if (this.hasChildren(tag)) {
                this.tagsList = this.allTagsWithoutReadyToMoveOnes.filter(t => t.parent === tag._id);
                this.listheader = tag.name;
                this.currentParentTag = tag;
                this.updateBackArrowState();
            }
        }
    }

    /**
     * Go back to tag's parent.
     */
    public goback() {
        if (this.currentParentTag.parent) {
            this.currentParentTag = this.findTag(this.currentParentTag.parent);
            this.tagsList = this.allTagsWithoutReadyToMoveOnes.filter(t => t.parent === this.currentParentTag._id);
            this.listheader = this.currentParentTag.name;
            this.updateBackArrowState();
        }
    }

     /**
     * Used to search a tag, in this mode no drill down is needed.
     */
    public searchTags(event) {
        if (!this.isCanceling) {
            this.isSearching = true;
            let val = event.target.value;
            // if the value is an empty string don't filter the items
            if (val && val.trim() !== '') {
                this.tagsList = this.allTagsWithoutReadyToMoveOnes.filter(t => t.name.search(new RegExp(val.trim(), "i")) !== -1);
                this.listheader = "Search Result";
                // TODO maybe will create a brand new search result view in a hearder tree structure.          
            }
        }
        this.isCanceling = false;
    }

    /**
     * Used to cancel a search.
     */
    public cancelSearch() {
        this.isCanceling = true;
        this.isSearching = false;
        this.resetTagListToRoot();
    }
    private hasChildren(tag) {
        return this.ramdata.getAllTagItems().some(t => t.parent === tag._id);
    }

    private getChildren(tag) {
        return this.ramdata.getAllTagItems().filter(t => t.parent === tag._id);
    }

    private findTag(tagId) {
        return this.ramdata.getAllTagItems().find(t => t._id === tagId);
    }

    private updateBackArrowState() {
        this.currentParentTag.parent ?
            this.isnotRootTag = true : this.isnotRootTag = false;
    }

    private resetTagListToRoot() {
        this.allTagsWithoutReadyToMoveOnes = this.ramdata.getAllTagItems().filter(t => {
            return !arrayContains(this.needToMoveTags,t);
        });
        this.tagsList = this.allTagsWithoutReadyToMoveOnes.filter(t => t.parent === 'root');
        this.listheader = this.rootTag.name;
        function arrayContains(array,item){
            return array.some(i => i._id ===item._id)
        }
    }

}
