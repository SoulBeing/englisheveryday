import {Component} from '@angular/core';
import {NavController, MenuController, Storage, LocalStorage, Platform} from 'ionic-angular';
import {TabsPage} from '../tabs/tabs';
import {TranslatePipe, TranslateService} from "ng2-translate/ng2-translate";
import {IPCService} from '../../providers/ipc.service';
import {InAppBrowser} from 'ionic-native';
declare var cordova;

interface Slide {
  title: string;
  description: string;
  image: string;
}

@Component({
  templateUrl: 'build/pages/tutorial/tutorial.html',
  pipes: [TranslatePipe]
})
export class TutorialPage {
  slides: Slide[];
  showSkip = true;
  local: Storage = new Storage(LocalStorage);

  constructor(private nav: NavController, 
  private menu: MenuController,
  private ipc:IPCService,
  private translate: TranslateService,
  private platform:Platform
  ) {}

  launch(url) {
      this.platform.ready().then(() => {
          new InAppBrowser(url, "_self", "location=true");
      });
  }

  startApp() {
    this.nav.push(TabsPage);
  }
  toggleLanguage() {
    let isChinese = this.ipc.getIsChinese();
    isChinese = !isChinese;
    this.ipc.setIsChinese(isChinese);
    this.local.set("defaultLanguage", isChinese ? "cn" : "en");
    this.translate.use(isChinese ? "cn" : "en");
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd;
  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

}
