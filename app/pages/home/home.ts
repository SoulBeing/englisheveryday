import {Component} from '@angular/core';
import {IPCService} from '../../providers/ipc.service';
import {NavController, NavParams, PopoverController, ViewController} from 'ionic-angular';
import {RamDataService} from '../../providers/ramdata.service';
import {TranslatePipe} from "ng2-translate/ng2-translate";

declare var marked: any;

@Component({
    templateUrl: 'build/pages/home/home.html',
    pipes: [TranslatePipe]
})
export class Home {
    private setting = { showTagsHelper: true };
    private adviseLearningItems = [];
    private allAdviceLearningItems = {};
    private channels;  // Used for providing options for popover
    private seletedChannel: any = {  // seletedChannel have to be an object to hold the value back and forth.
        selected: ""
    };

    constructor(
        private ipc: IPCService,
        private nav: NavController,
        private ramdata: RamDataService,
        private popoverController: PopoverController
    ) { }
    private ionViewWillEnter() {
        let that = this;
        this.ramdata.getAdviceLearningItems().
            then(data => {
                if (data) {
                    that.resetAdviceLearningItems(data)
                }
            });
    }

    private resetAdviceLearningItems(data){
        this.allAdviceLearningItems = data;
        this.channels = Object.keys(data);
        let lastChannel = this.ipc.getLastChannel();
        let currentChannel = lastChannel ? lastChannel : this.channels[0];
        this.seletedChannel = { selected: currentChannel };
        this.ipc.setLastChannel(this.channels[0]);
        this.adviseLearningItems = data[this.seletedChannel.selected];
    }

    public renderMarkedWithHint(sentence){
        var sentenceWithHint = sentence.replace(/ *\[[^\]]*\]*/g, "")
        return marked(sentenceWithHint);
    }
    public renderMarkedWithoutHint(sentence){
        var sentenceWithoutHint = sentence.replace(/ *\[[^\]]*\]*| *\([^)]*\)*|\_\_/g, "")
        return marked(sentenceWithoutHint);
    }

    public hasWholeTranslation(sentence){
        var wholeTranslationReg =  / *\[([^\]]*)\]*/g;
        var wholeTranslation = wholeTranslationReg.exec(sentence);
        return wholeTranslation && wholeTranslation[1];
    }

    public hasHintTranslation(sentence){
        var hintTranslationReg =  / *\([^)]*\)*/g;
        var wholeTranslation = hintTranslationReg.exec(sentence);
        return wholeTranslation;
    }

    public showTranslation(item){
        item.showWhole = true;
    }
    public showHint(item){
        item.showHint = true;
    }

    /**
     * Only Used for changing channel.
     */
    public listenOnChannelChanges() {
        let lastChannel = this.ipc.getLastChannel();
        if (lastChannel !== this.seletedChannel.selected) {
            this.adviseLearningItems = this.allAdviceLearningItems[this.seletedChannel.selected];
            if (this.seletedChannel.selected) {
                this.ipc.setLastChannel(this.seletedChannel.selected);
            }
        }
        return true;
    }

    public learnThisone(addviceLearningItem) {
        let toSaveLearningItem;
        toSaveLearningItem = {
            'isNew': true,
            'sentence': addviceLearningItem.sentence
        };
        if (addviceLearningItem.possibleTags.length > 0 && this.setting.showTagsHelper) {
            toSaveLearningItem.possibleTags = addviceLearningItem.possibleTags
        }
        this.ipc.publishEvent('swith:toinput');
        this.ipc.setTransferObject(toSaveLearningItem);
    }

    doInfinite(infiniteScroll) {
        let that =this;
        this.ipc.needMoreAdviceLeanringItem();
        this.ramdata.setOldestDateOfAdviceLearningItem();
        this.ramdata.getAdviceLearningItems().then(data => {
            if(data === "no_more_data"){
                infiniteScroll.enable(false);
            }else{
                that.resetAdviceLearningItems(data)
                infiniteScroll.complete();
            }
        });  
    }

    public presentPopover(myEvent) {
        let popover = this.popoverController.create(PopoverPage, {
            setting: this.setting,
            channels: this.channels,
            selectedChannel: this.seletedChannel
        });
        popover.present({
            ev: myEvent
        });
    }
}

@Component({
    template: `
    <ion-list>
      <ion-list-header>{{ "nested.searchPopupTitle" | translate }}</ion-list-header>
        <ion-item (click)="close()">
            <ion-label>{{ "nested.showAdviceTags" | translate }}</ion-label>
            <ion-toggle [(ngModel)]="setting.showTagsHelper"></ion-toggle>
        </ion-item>
    </ion-list>
    <ion-list radio-group [(ngModel)]="selectedChannel.selected">
        <ion-list-header>
            {{ "nested.channels" | translate }}
        </ion-list-header>
        <ion-item *ngFor="let channel of channels" (click)="close()">
            <ion-label>{{channel}}</ion-label>
            <ion-radio value="{{channel}}"></ion-radio>
        </ion-item>
    </ion-list>
  `,
    pipes: [TranslatePipe]
})
class PopoverPage {
    private setting: any;
    private channels: any;
    private selectedChannel: any;
    constructor(private navParams: NavParams,private viewCtrl: ViewController) { }

    ngOnInit() {
        if (this.navParams.data) {
            this.setting = this.navParams.data.setting;
            this.channels = this.navParams.data.channels;
            this.selectedChannel = this.navParams.data.selectedChannel;
        }
    }
    close() {
        this.viewCtrl.dismiss();
    }
}