import {NavParams, NavController} from 'ionic-angular';
import {Home} from '../home/home';
import {Review} from '../review/review';
import {Input} from '../input/input';
import {Search} from '../search/search';
import {Component} from '@angular/core';
import {AuthService} from '../../providers/auth.service';
import {ProfilePage} from '../profile/profile';
import {TranslatePipe, TranslateService} from "ng2-translate/ng2-translate";
import {IPCService} from '../../providers/ipc.service';

@Component({
  templateUrl: 'build/pages/tabs/tabs.html',
  pipes: [TranslatePipe],
  providers: [Review],
})
export class TabsPage {
  tab1Root: any = Home;
  tab2Root: any = Review;
  tab3Root: any = Input;
  tab4Root: any = Search;
  mySelectedIndex: number;
  needToReviewItemsCount;
  private _badgeStateCheckTimeout;

  profilePage = { title: 'Account', component: ProfilePage, icon: 'person' };

  constructor(navParams: NavParams,
    private auth: AuthService,
    private nav: NavController,
    private translate: TranslateService,
    private ipc: IPCService) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

  // This method will be called all the time, to check the needToReviewItems number
  public checkBadgeState() {
    this.needToReviewItemsCount = this.ipc.getNeedToReviewNumber();
    var badgeElement = document.querySelector('.tab-badge');
    if (this.needToReviewItemsCount > 0) {
      badgeElement && badgeElement.classList.remove('hide');
    } else {
      badgeElement && badgeElement.classList.add('hide');
    }
    return true;
  }
}
