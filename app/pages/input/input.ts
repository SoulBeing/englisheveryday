import {ModalController, NavParams, ViewController, NavController, ToastController, AlertController} from 'ionic-angular';
import {Component} from '@angular/core';
import {RamDataService} from '../../providers/ramdata.service';
import {TranslateService} from '../../providers/translate.service';
import {Autosize} from '../../directives/autosize.directive';
import {Tags} from '../tags/tags';
import {IPCService} from '../../providers/ipc.service';
import {TranslatePipe} from "ng2-translate/ng2-translate";
import {TextToSpeech} from 'ionic-native';

@Component({
    templateUrl: 'build/pages/input/input.html',
    providers: [TranslateService],
    directives: [Autosize],
    pipes: [TranslatePipe]
})
export class Input {
    private learningItem;
    private isNew = true;
    private action = 'Add';
    private words = [{ thing: '' }];
    private wordgroups = [{ thing: '' }];
    private tags = [];
    private sentence;
    private note = "";

    // These four variable will control whether to show the input box of each section.
    private isSentenceNeed: boolean = false;
    private isNoteNeed: boolean = false;
    private isWordNeed: boolean = true;
    private isWordGroupNeed: boolean = true;
    private isTagSectionNeed: boolean = true;

    private sentenceIconName = "ios-arrow-down";
    private noteIconName = "ios-arrow-down";
    private wordIconName = "ios-arrow-up";
    private wordgroupIconName = "ios-arrow-down";

    private possibleTagsfromAdvice = [];
    private badgeAccount = 0;

    private translation: String;

    constructor(private viewCtrl: ViewController,
        private nav: NavController,
        private navParams: NavParams,
        private ramdata: RamDataService,
        private translateService: TranslateService,
        private ipc: IPCService,
        private alertController: AlertController,
        private toastController: ToastController,
        public modalCtrl: ModalController) {
    }

    ionViewWillEnter() {
        this.learningItem = {};
        let recevingItem = this.ipc.getTransferObject();
        this.ipc.clearTransferObject();
        if (recevingItem.isNew) {
            // This is for advice sentence learning cases.
            recevingItem.sentence && (this.sentence = recevingItem.sentence.replace(/ *\[[^\]]*\]*| *\([^)]*\)*|\_\_/g, "")) && (this.isSentenceNeed = true);
            if (recevingItem.possibleTags) {
                recevingItem.possibleTags && (this.possibleTagsfromAdvice = recevingItem.possibleTags);
                this.badgeAccount = this.possibleTagsfromAdvice.filter((each) => {
                    return !!each;
                }).length;
            }
        } else {
            this.isNew = false;
            this.action = 'Edit';
            this.learningItem = recevingItem;
            this.learningItem.words && (this.words = this.objectifyArray(this.learningItem.words));
            this.learningItem.wordgroups && (this.wordgroups = this.objectifyArray(this.learningItem.wordgroups));
            this.learningItem.sentence && (this.sentence = this.learningItem.sentence) && (this.isSentenceNeed = true);
            this.learningItem.tags && (this.tags = this.learningItem.tags);
            this.learningItem.note && (this.note = this.learningItem.note) && (this.isNoteNeed = true);
        }
    }
    ionViewDidEnter() {
        var sentenceELement: any = document.querySelector("#input_sentence");
        if(sentenceELement){
            sentenceELement.style.overflow = 'hidden';
            sentenceELement.style.height = 'auto';
            sentenceELement.style.height = sentenceELement.scrollHeight + "px";
        }
    }

    public save() {
        // This is the stucture of learningItem
        let that = this;
        let wordsStringArray = this.unobjectifyArray(this.words);
        let wordgroupsStringArray = this.unobjectifyArray(this.wordgroups);

        // Check existing first, save only when exists.
        wordsStringArray && (this.learningItem.words = wordsStringArray);
        wordgroupsStringArray && (this.learningItem.wordgroups = wordgroupsStringArray);
        this.tags && (this.learningItem.tags = this.tags);
        this.sentence && (this.learningItem.sentence = this.sentence);
        this.note && (this.learningItem.note = this.note);

        function isWorthSaving() {
            return wordsStringArray[0] || wordgroupsStringArray[0] || that.sentence;
        };
        if (isWorthSaving()) {
            if (this.isNew) {
                let date = Date.now();
                this.learningItem.lastReviewDate = date;
                this.learningItem.reviewedTimes = 0;
                this.learningItem.inputDate = date;
                this.learningItem._id = date + "";
                this.ramdata.saveNewLearningItem(this.learningItem);
            } else {
                this.ramdata.updateLearningItem(this.learningItem);
                // this.nav.pop();
            }
            this.backToDefaultState();
            this.showSimpleSnackbar();
        }
    }
    public noteHelp(inputFromHelper) {
        if (inputFromHelper) {
            this.isNoteNeed = true;
            let separator = "    ";
            if (this.note) {
                this.note = this.note + separator;
            }
            this.note = this.note + inputFromHelper + ":" + separator;
        }
    }

    private unobjectifyArray(thingObjectItemArray) {
        var result = [];
        thingObjectItemArray.forEach(item => {
            if (item.thing !== "") {
                result.push(item.thing);
            }
        })
        return result ? result : null;
    }

    private objectifyArray(StringItemArray) {
        var result = [];
        StringItemArray.forEach(item => {
            result.push({ thing: item });
        })
        return result;
    }

    backToDefaultState() {
        this.learningItem = {};
        this.sentence = ""
        this.note = ""
        this.words = [{ thing: '' }];
        this.wordgroups = [{ thing: '' }];
        this.possibleTagsfromAdvice = [];
        this.tags = [];
        this.isNew = true;
    }

    /**
     * This method will called when user click the items control button.Cases are:
     * not enterred anything: flip or fold
     * enterred one : add one more
     * have multiple opened, but the last is empty: remove the last one
     */
    oneMoreWordOrRemove() {
        if (this.words[0] && this.words[0].thing === "") {
            // show the word input box for the first time
            this.isWordNeed = !this.isWordNeed;
        } else if (this.words.length > 1 && this.words[this.words.length - 1].thing === "") {
            this.words.pop();
        } else {
            this.words.push({ thing: '' });
        }
    }
    /**
     * This method will be called at every tick, it will control the + _ icon's image.
     * Note: This method is bind to *ngIf, so that it will be called at every tick to run the code, but we return "true" always.
     * Note: All we want to achieve here is change the icon's image in different states.
     * Cases ares:( the cases will definitly corresponding to oneMoreWordOrRemove() method )
     * not enterred anything: ios-arrow-up or ios-arrow-down
     * enterred one : add-circle
     * have multiple opened, but the last is empty: remove-circle
     */
    checkWordsState() {
        let wordsLength = this.words.length;
        if (wordsLength === 1 && this.words[0] && this.words[0].thing === "") {
            this.wordIconName = this.isWordNeed ? "ios-arrow-up" : "ios-arrow-down";
        } else if (this.words[wordsLength - 1] && this.words[wordsLength - 1].thing === "") {
            this.wordIconName = "remove";
        } else if (this.words[wordsLength - 1] && this.words[wordsLength - 1].thing !== "") {
            this.wordIconName = "add";
        }
        return true;
    }

    addOrRemoveSentence() {
        this.isSentenceNeed = !this.isSentenceNeed;
        this.sentenceIconName = this.isSentenceNeed ? "ios-arrow-up" : "ios-arrow-down";
    }

    addOrRemoveNote() {
        this.isNoteNeed = !this.isNoteNeed;
        this.noteIconName = this.isNoteNeed ? "ios-arrow-up" : "ios-arrow-down";
    }

    tts(){
        if(this.sentence){
           TextToSpeech.speak(this.sentence); 
        }
    }

    oneMoreWordgroupOrRemove() {
        if (this.wordgroups[0] && this.wordgroups[0].thing === "") {
            // show the word input box for the first time
            this.isWordGroupNeed = !this.isWordGroupNeed;
        } else if (this.wordgroups.length > 1 && this.wordgroups[this.wordgroups.length - 1].thing === "") {
            this.wordgroups.pop();
        } else {
            this.wordgroups.push({ thing: '' });
        }
    }
    checkWordGroupsState() {
        let wordgroupsLength = this.wordgroups.length;
        if (wordgroupsLength === 1 && this.wordgroups[0] && this.wordgroups[0].thing === "") {
            this.wordgroupIconName = this.isWordGroupNeed ? "ios-arrow-up" : "ios-arrow-down";
        } else if (this.wordgroups[wordgroupsLength - 1] && this.wordgroups[wordgroupsLength - 1].thing === "") {
            this.wordgroupIconName = "remove";
        } else if (this.wordgroups[wordgroupsLength - 1] && this.wordgroups[wordgroupsLength - 1].thing !== "") {
            this.wordgroupIconName = "add";
        }
        return true;
    }

    translate(word) {
        if (word) {
            this.translateService.translateYouDaoAPI(word).subscribe(
                data => {
                    let translationString = word;
                    let isChinese = this.ipc.getIsChinese();
                    if (data.basic) {
                        translationString = (translationString + " ") + (data.basic.phonetic ? '/' + data.basic.phonetic + '/' : "");
                        data.basic.explains.forEach(explain => {
                            translationString = translationString + "<br>" + explain;
                        });
                    } else {
                        translationString = (translationString + " ") + (isChinese ? '找不到对应翻译!' : 'No translation can be found!');
                    }
                    this.showTranslation({
                        translate:translationString,
                        word:word
                    });
                },
                err => {
                    //console.log(err)
                },
                () => { }
            );
        }
    }

    addTags() {
        let isChinese = this.ipc.getIsChinese();
        let that = this;
        if (that.possibleTagsfromAdvice.length > 0) {
            let alert = that.alertController.create();
            alert.setTitle(isChinese ? '选择需要的标签' : 'Choose the tags you prefer');
            for (let i = 0; i < that.possibleTagsfromAdvice.length; i++) {
                if (that.possibleTagsfromAdvice[i]) {
                    alert.addInput({
                        type: 'checkbox',
                        label: that.possibleTagsfromAdvice[i],
                        value: i + that.possibleTagsfromAdvice[i],
                        checked: true
                    });
                }
            }
            alert.addButton(isChinese ? '取消' : 'Cancel');
            alert.addButton({
                text: isChinese ? '确认' : 'Okay',
                handler: data => {
                    let tagParentType = ["source", "theme", "content", "root"];  // THis corresponding to the _id of default tags. and in order of predefined possibleTagsFromAcvice
                    for (let k = 0; k < data.length; k++) {
                        let typeIndex = data[k].slice(0, 1);
                        let tagName = data[k].slice(1);
                        let newTag = {
                            _id: Date.now() + "",
                            name: tagName,
                            parent: tagParentType[typeIndex]   // 0 -> source , 1 -> theme , 2 -> content , 3-> root; This is scrict.
                        };
                        let checkResponse = that.ramdata.normalizeTag(newTag);
                        that.tags.push(checkResponse.tag);
                        if (checkResponse.isNew) {
                            that.ramdata.saveTagItem(newTag);
                        }
                    }
                    that.possibleTagsfromAdvice = [];
                }
            });
            alert.present();
        } else {
            let modal = that.modalCtrl.create(Tags, this.tags);
            modal.present();
            modal.onDidDismiss((data: any[]) => {
                if (data) {
                    that.tags = data;
                }
            });
        }
    }
    deleteTag(tag) {
        this.tags = this.tags.filter(function (r) { return r.name != tag.name; });
    }
    showSimpleSnackbar() {
        let isChinese = this.ipc.getIsChinese();
        let toast = this.toastController.create({
            message: isChinese ? '搞定' : 'Saved',
            duration: 1000,
            position: 'top',
            dismissOnPageChange: true
        });

        toast.present();
    }
    showTranslation(translation:any) {
        let isChinese = this.ipc.getIsChinese();
        let alert = this.alertController.create({
            message: translation.translate,
            buttons: [
                {
                    text: isChinese ? '发音' : 'Read',
                    handler: () => {
                        TextToSpeech.speak(translation.word);
                        return false;
                    }
                },
                {
                    text: isChinese ? '确认' : 'OK',
                    role: 'cancel',
                    handler: () => {
                    }
                }
            ]
        });
        alert.present();
    }
}
