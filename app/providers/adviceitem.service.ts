import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
import {AuthHttp} from 'angular2-jwt';

@Injectable()
export class AdviceItemService {
    constructor(private authHttp: AuthHttp) {
    }

    getThreeAdvicedLearningItem(date) {
        var url = "https://englisheveryday.mybluemix.net/learningitem?date=" + encodeURI(date);     // Real one , no secured is needed
        // var url = "http://localhost:3000/learningitem?date=" + encodeURI(date);         // Localhost debuging case
        return this.authHttp.get(url).map(function (res) {
            return res.json();
        });
    }
}