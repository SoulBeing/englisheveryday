import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
import {AuthHttp} from 'angular2-jwt';
import {Headers} from '@angular/http';

@Injectable()
export class CloudService {
    constructor(private authHttp: AuthHttp) { }

    uploadToCloud(userId, wholeData) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // let url = "http://localhost:3000/secured/cloudbackup?userId=" + encodeURI(userId);  // Localhost testing URL
        var url = "https://englisheveryday.mybluemix.net/secured/cloudbackup?userId=" + encodeURI(userId);
        return this.authHttp.post(url, JSON.stringify(wholeData), { headers: headers })
            .map(function (res) {
                return res.status;
            });
    }

    downloadFromCloud(userId) {
        // let url = "http://localhost:3000/secured/cloudbackup?userId=" + encodeURI(userId);  // Localhost testing URL
        var url = "https://englisheveryday.mybluemix.net/secured/cloudbackup?userId=" + encodeURI(userId);
        return this.authHttp.get(url).map(function (res) {
            return res.json();
        });
    }
}