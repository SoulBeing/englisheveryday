import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class UpgradeService {
    private CURRENT_VERSION_ANDROID = 3;  // 0.0.3 = 3 ;   0.1.2 = 12 ; 1.2.3 = 123 Modify this only on Android Branch
    private CURRENT_VERSION_IOS = 1;      // 0.0.1                                  Modify this only on IOS Branch
    private APPSTORE_DOWNLOAD_LINK = "https://itunes.apple.com/us/app/nan-wang-ying-yu/id1143657386?ls=1&mt=8";
    private GOOGLEPLAY_DOWNLOAD_LINK = "https://play.google.com/store/apps/details?id=net.mybluemix.englisheveryday";
    private HOMEPAGE_DOWNLOAD_LINK = "https://englisheveryday.mybluemix.net/downloadandroid";
    constructor(private http: Http) { }

    private getCurrentVersion(platform){
        if(platform === "android"){
            return this.CURRENT_VERSION_ANDROID;
        }else if(platform === "ios"){
            return this.CURRENT_VERSION_IOS;
        }
    }

    public uploadToCloud(platform) {
        var url = "https://englisheveryday.mybluemix.net/upgrade/" + platform +"?version_self=" + this.getCurrentVersion(platform);
        // let url = "http://localhost:3000/upgrade/" + platform +"?version_self=" + this.getCurrentVersion(platform);
        return this.http.get(url).map(function (res) {
            return res.json();
        });
    }
    public getAppStoreDownloadLink(){
        return this.APPSTORE_DOWNLOAD_LINK;
    }
    public getGooglePlayDownloadLink(){
        return this.GOOGLEPLAY_DOWNLOAD_LINK;
    }
    public getHomePageDownloadLink(){
        return this.HOMEPAGE_DOWNLOAD_LINK;
    }


}