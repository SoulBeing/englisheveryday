import {Injectable} from '@angular/core';
import {Platform} from 'ionic-angular';

declare var window;

@Injectable()
export class LearningItemService {
    private _db: any;
    private _persistentItems;

    initLeanringItemPersistentDatabase(db) {
        this._db = db;
        // this._db = new PouchDB("learningItem", { adapter: 'websql' });
    }
    isDatabaseReady(){
        let MAX_ATTEMP = 3;
        let that = this;
        function processWhile(){
            return that._db.info()
            .then(result =>{
                if(result.db_name === "learningItem"){
                    return true;
                }
            }).catch(err =>{
                if(MAX_ATTEMP-- > 0){
                    processWhile();
                }else{
                    return Promise.reject(err);
                }
            });
        }
        return processWhile();
    }

    deleteLearningItemPersistentDatabase() {
        this._db.destroy().then(function () {
//        	console.log("Database deleted") 
        	});
    }

    addItem(item) {
        return this._db.put(item).then(function (response) {
            // handle response
        }).catch(function (err) {
//            console.log(err);
        });
    }
    sync(item) {
        let that = this;
        return that._db.get(item._id)
        .then(function (doc) {
            item._rev = doc._rev;
            return that._db.put(item).then(function (response) {
                return response;
            });
        }).catch(function (err) {
            delete item._rev;
            return that._db.put(item).then(function (response) {
                return response;
            }).catch(function (err) {
//                console.log(err);
            });
        });
    }
    update(item) {
        let that = this;
        that._db.get(item._id).then(function (doc) {
            item._rev = doc._rev;
            return that._db.put(item);
        }).then(function (response) {
            // handle response
        }).catch(function (err) {
//            console.log(err);
        });
    }
    delete(item) {
        return this._db.remove(item);
    }
    getAll() {
        let that = this;
        if (!that._persistentItems && that._db) {
            return that._db.allDocs({ include_docs: true })
                .then(docs => {
                    that._persistentItems = docs.rows;

                    that._db.changes({ live: true, since: 'now', include_docs: true })
                        .on('change', function (change) {
                            var index = that.findIndex(that._persistentItems, change.id);
                            var item = that._persistentItems[index];

                            if (change.deleted) {
                                if (item) {
                                    that._persistentItems.splice(index, 1);   // delete
                                }
                            } else {
                                if (item && item._id === change.id) {
                                    that._persistentItems[index] = change.doc; //update
                                } else {
                                    that._persistentItems.splice(index, 0, change.doc);
                                }
                            }
                        });
                    return that._persistentItems;
                });
        } else {
            return Promise.resolve(that._persistentItems);
        }
    }

    private findIndex(array, id) {
        var low = 0, high = array.length, mid;
        while (low < high) {
            mid = (low + high) >>> 1;
            array[mid]._id < id ? low = mid + 1 : high = mid;
        }
        return low;
    }
}