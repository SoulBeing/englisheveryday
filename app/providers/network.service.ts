import {Injectable} from '@angular/core';

@Injectable()
export class NetworkService {
    corsPrefix: String = "localhost:1337/";
    isCors: boolean = false;
    getCorsPrefix(){
        return this.isCors ? this.corsPrefix : "";
    }
}