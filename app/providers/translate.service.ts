import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
import {NetworkService} from './network.service';

declare var MD5: any;

@Injectable()
export class TranslateService {
    constructor( private http: Http,  private networkService: NetworkService) {
    }

    translateYouDaoAPI(inputValue) {
        var url = "http://" + this.networkService.getCorsPrefix() + "fanyi.youdao.com/openapi.do?keyfrom=EnglishEveryday&key=1705329391&type=data&doctype=json&version=1.1&q=" + encodeURI(inputValue);
        return this.http.get(url).map(function (res) {
            return res.json();
        });
    }

    translateBaiduAPI(inputValue){
        let salt = (new Date).getTime();
        let appid = '20160313000015378';
        let key = 'SukQxajY0T2kmVabUSpl';
        let str1 = appid + inputValue + salt + key;
        let sign = MD5(str1);
        var url = "http://" + this.networkService.getCorsPrefix() + "api.fanyi.baidu.com/api/trans/vip/translate?q=" + encodeURI(inputValue)+"&from=en&to=zh&appid=" + appid + "&salt=" + salt + "&sign=" + sign;
        return this.http.get(url).map(function (res) {
            return res.json();
        });
    }

    translateWordandPhraseforModal(inputs){
        var that = this;
        return new Promise(function(fulfill,reject){
            return that.translateBaiduAPI(inputs).subscribe(
                data => {
                        fulfill(data.trans_result);
                    },
                err => reject(err),
                () => { }
            );
        });
    }
}