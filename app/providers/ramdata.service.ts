import {Injectable} from '@angular/core';
import {LearningItemService} from './learningItem.service';
import {TagPersistentService} from './tagpersistent.service';
import {NgZone} from '@angular/core';
import {IPCService} from './ipc.service';
import {AdviceItemService} from './adviceitem.service';


@Injectable()
export class RamDataService {
    private wholeLearningItemData = [];
    private wholeTagsData = [];
    private adviceLearningItemsOfAllDate: any = [];
    private oldestDateOfAdviceLearningItem;

    constructor(
        private tagItemPersistant: TagPersistentService,
        private learnItemPersistant: LearningItemService,
        private zone: NgZone,
        private ipc: IPCService,
        private adviceItem: AdviceItemService) { }

    /**
     * Used to initialize data, this process will take a few seconds
     */
    public initializeAllData() {
        let that = this;
        let loadLearningItemPromise =
            this.learnItemPersistant.getAll()
                .then(data => {
                    this.zone.run(() => {
                        //TODO this long process will eventuall executed in the loading page.
                        this.wholeLearningItemData = this.getPureData(data);
                        this.sortLearningItemsByCreatingDate(this.wholeLearningItemData);
                    });
                })
                .catch(console.error.bind(console));

        let loadTagsPromise =
            this.tagItemPersistant.getAll()
                .then(data => {
                    this.zone.run(() => {
                        //TODO this long process will eventuall executed in the loading page.                  
                        this.wholeTagsData = this.getPureData(data);
                        let rootTag = { _id: "root", name: "" };
                        if (!this.wholeTagsData.some(t => t._id === 'root')) {
                            this.wholeTagsData.unshift(rootTag);
                        }
                    });
                })
                .catch(console.error.bind(console));
        return Promise.all([loadLearningItemPromise, loadTagsPromise])
            .then(function () {
                return that.feedLearningItemWithTags();
            });
    }
    isDataBaseReady() {
        return Promise.all([this.tagItemPersistant.isDatabaseReady(), this.learnItemPersistant.isDatabaseReady()])
            .then(results => {
                if (results[0] && results[1]) {
                    return true;
                }
            }).catch(err => {
                return false;
            });
    }
    /**
     * Since in original this.wholeLearningItemData, tags only saved with _id, so here need to flesh the tags with its full content from this.wholeTagsData;
     */
    private feedLearningItemWithTags() {
        let that = this;
        this.wholeLearningItemData = this.wholeLearningItemData.map((learningItem) => {
            return learningItem = fleshwithTag(learningItem)
        });
        function fleshwithTag(learningItem) {
            if (learningItem.tags && learningItem.tags[0] && typeof learningItem.tags[0] === "string") {
                let fleshedTags = []
                for (var i = 0; i < learningItem.tags.length; i++) {
                    let foundTag = findTag(learningItem.tags[i]); // This tag is the tag._id saved
                    if (foundTag) {
                        fleshedTags.push(foundTag);
                    }
                }
                learningItem.tags = fleshedTags
            }
            return learningItem;
        }
        function findTag(tagId) {
            return that.wholeTagsData.find(t => t._id === tagId);
        }
    }
    private sortLearningItemsByCreatingDate(wholeLearningItemData) {
        wholeLearningItemData.sort(function (a, b) {
            return b.inputDate - a.inputDate;
        })
    }

    public deletebothDatabase() {
        this.wholeLearningItemData = [];
        this.wholeTagsData = [];
        this.learnItemPersistant.deleteLearningItemPersistentDatabase();
        this.tagItemPersistant.deleteTagPersistentDatabase();
    }

    // Methods for Learning Items
    public getAllLearningItems() {
        return this.wholeLearningItemData;
    }
    public saveNewLearningItem(newItem) {
        this.wholeLearningItemData.unshift(newItem);
        setTimeout(this.learnItemPersistant.addItem(this.unfleshTag(newItem)), "");
    }
    /**
     * 
     */
    private syncLearningItem(items) {
        let that = this;
        return new Promise(function (fulfill, reject) {
            // feed Ram with sync data
            if (!that.checkIfIDExist(that.wholeLearningItemData, items[0])) {
                that.wholeLearningItemData.unshift(items[0]);
            }
            fulfill();
        }).then(function () {
            // feed persistant with sync data
            return that.learnItemPersistant.sync(items[0]);  // already unfleshed
        }).then(function (result) {
            items.shift();
            return items;
        });
    }
    private checkIfIDExist(array, item) {
        return array.some((each) => {
            return each._id === item._id;
        })
    }
    public syncFromWholeLearningItemsData(learningItems) {
        let that = this;
        return Promise.resolve(learningItems).then(function (toSaveItems) {
            return that.syncLearningItem(toSaveItems)
                .then(function (leftingUnSyncedItems) {
                    if (leftingUnSyncedItems.length > 0) {
                        return that.syncFromWholeLearningItemsData(leftingUnSyncedItems);
                    }
                })
        });
    }
    public syncFromWholeTagsData(tags) {
        let that = this;
        return Promise.resolve(tags).then(function (toSaveItems) {
            return that.syncTagItem(toSaveItems)
                .then(function (leftingUnSyncedItems) {
                    if (leftingUnSyncedItems.length > 0) {
                        return that.syncFromWholeTagsData(leftingUnSyncedItems);
                    }
                })
        });
    }
    public syncFromData(data) {
        let that = this;
        return Promise.all([that.syncFromWholeLearningItemsData(data.learningItems), that.syncFromWholeTagsData(data.tags)])
            .then(function () {
                that.sortLearningItemsByCreatingDate(that.wholeLearningItemData);
                return that.feedLearningItemWithTags();
            })
            .catch(function (err) {
                //                console.log(err);
            })
    }
    public updateLearningItem(item) {
        // Update the item in wholeLearningItemData
        this.wholeLearningItemData.some(oldItem => {
            if (oldItem._id === item._id) {
                oldItem = item;
                return true;
            }
        });
        // Update the item in disk
        this.learnItemPersistant.update(this.unfleshTag(item));
    }
    private unfleshTag(learningItem) {
        let result;
        result = {};   // It's important to seperate these two lines, declare first and assignment.
        result = clone(learningItem);
        result.tags = learningItem.tags.map(function (tag) {
            return tag._id;
        })
        return result;
        function clone(obj) {
            if (null == obj || "object" != typeof obj) return obj;
            var copy = obj.constructor();
            for (var attr in obj) {
                if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
            }
            return copy;
        }
    }
    public allUnfleshedLearningItems() {
        let that = this;
        return this.wholeLearningItemData.map(each => {
            return that.unfleshTag(each);
        });
    }
    public deleteLearningItem(item) {
        this.wholeLearningItemData = this.wholeLearningItemData.filter(function (r) { return r._id != item._id; });
        setTimeout(this.learnItemPersistant.delete(item), "");
    }
    // Methods for Tag Items
    public getAllTagItems() {
        return this.wholeTagsData;
    }
    public saveTagItem(newtag) {
        this.wholeTagsData.unshift(newtag);
        setTimeout(this.tagItemPersistant.addItem(newtag), "");
    }
    private syncTagItem(tags) {
        let that = this;
        return new Promise(function (fulfill, reject) {
            if (!that.checkIfIDExist(that.wholeTagsData, tags[0])) {
                that.wholeTagsData.unshift(tags[0]);
            }
            fulfill();
        }).then(function () {
            return that.tagItemPersistant.sync(tags[0]);
        }).then(function (result) {
            // Check result
            tags.shift();
            return tags;
        });
    }
    public removeTag(tag) {
        let that = this;
        this.wholeTagsData = this.wholeTagsData.filter(function (r) { return r._id != tag._id; });
        // Only need to delete the tag from the learningItem at the Ram, no need to think about persistant learningItem's tag data.
        // This is all because, when initializing the tags of a learningItem, the unfind tag won't be shown on the screen even though it won't be 
        // deleted until next learningItem update(in the update, a new whole item will be up in, that will cover over the previous deleted tag.)
        removeTagfromLearningItemRam(tag);
        setTimeout(this.tagItemPersistant.delete(tag), "");
        function removeTagfromLearningItemRam(tag) {
            that.wholeLearningItemData.forEach(learningItem => {
                let index = indexOfTagArray(learningItem.tags, tag); // Will be undefined if no such tag of this learningItem.tags.
                if (index >= 0) {
                    learningItem.tags.splice(index, 1);
                }
            });
            function indexOfTagArray(tagArray, tag) {
                for (let i = 0; i < tagArray.length; i++) {
                    if (tag._id === tagArray[i]._id) {
                        return i;
                    }
                }
            }
        }
    }
    public updateTagItem(item) {
        // Update the item in wholeTagsData
        this.wholeTagsData.some(oldItem => {
            if (oldItem._id === item._id) {
                oldItem = item;
                return true;
            }
        });
        this.tagItemPersistant.update(item);
    }
    public updateTagItemTOPersistant(item) {
        setTimeout(this.tagItemPersistant.update(item), "");
    }
    /**
     * This method is used to check if this tag has an alise of the same parent.
     */
    public normalizeTag(tag) {
        let existingAliseTag = this.wholeTagsData.find(t => {
            return t.parent === tag.parent && t.name === tag.name;
        });
        return existingAliseTag ? { 'tag': existingAliseTag, 'isNew': false } : { 'tag': tag, 'isNew': true };
    }
    private getPureData(sqlOriginalDataArray) {
        let result = [];
        for (let i = 0; i < sqlOriginalDataArray.length; i++) {
            result.push(sqlOriginalDataArray[i].doc);
        }
        return result;
    }

    public updateReviewItemsData() {
        let isChinese = this.ipc.getIsChinese();
        let reviewItems = [
            { items: [], placeholder: isChinese ? '首次' : 'First Time' },         //reviewItems[0]
            { items: [], placeholder: isChinese ? '第二次' : 'Second Time' },       //reviewItems[1]
            { items: [], placeholder: isChinese ? '第三次' : 'Third Time' },       //reviewItems[2]
            { items: [], placeholder: isChinese ? '第四次' : 'Forth Time' },       //reviewItems[3]
            { items: [], placeholder: isChinese ? '第五次' : 'Fifth Time' },       //reviewItems[4]
            { items: [], placeholder: isChinese ? '最后一次' : 'Last Time' }       //reviewItems[5]
        ];
        let newInputsItems;
        let wordReviewItems;
        let count = 0;
        clearAllCache();
        function clearAllCache() {
            newInputsItems = [];
            wordReviewItems = [];
            reviewItems.forEach(group => {
                group.items = [];
            });
        }
        let tempRamdomArray = [];
        this.getAllLearningItems().forEach(item => {
            switch (item.reviewedTimes) {
                case 0:
                    if (this.daysBetweenToday(item.lastReviewDate) >= 1) {
                        reviewItems[0].items.push(item);
                        count++;
                    } else {
                        newInputsItems.push(item);
                    }
                    break;
                case 1:
                    if (this.daysBetweenToday(item.lastReviewDate) >= 1) {
                        reviewItems[1].items.push(item);
                        count++;
                    }
                    break;
                case 2:
                    if (this.daysBetweenToday(item.lastReviewDate) >= 2) {
                        reviewItems[2].items.push(item);
                        count++;
                    }
                    break;
                case 3:
                    if (this.daysBetweenToday(item.lastReviewDate) >= 3) {
                        reviewItems[3].items.push(item);
                        count++;
                    }
                    break;
                case 4:
                    if (this.daysBetweenToday(item.lastReviewDate) >= 5) {
                        reviewItems[4].items.push(item);
                        count++;
                    }
                    break;
                case 5:
                    if (this.daysBetweenToday(item.lastReviewDate) >= 8) {
                        reviewItems[5].items.push(item);
                        count++;
                    }
                    break;
                default:
                    break;
            }
        });
        wordReviewItems = this.updataWordsForReview();
        this.ipc.setNeedToReviewNumber(count);
        return {
            'reviewItems': reviewItems,
            'newInputsItems': newInputsItems,
            'wordReviewItems': wordReviewItems,
            'count': count
        };
    }
    public updataWordsForReview() {
        let wordReviewItems = [];
        let learningItemsAcount = this.getAllLearningItems().length;
        let selectedIndex = [];
        let NEEDED_WORD_NUMBER = 10;
        if (learningItemsAcount > 0) {
            while (wordReviewItems.length < NEEDED_WORD_NUMBER && selectedIndex.length < learningItemsAcount) {
                let indexOfLearningItems = getRandomIndex(learningItemsAcount);
                let thislearningItem = this.getAllLearningItems()[indexOfLearningItems];
                if (selectedIndex.indexOf(indexOfLearningItems) === -1) {
                    selectedIndex.push(indexOfLearningItems);
                    if (thislearningItem.words.length > 0) {
                        let singleWord = thislearningItem.words[getRandomIndex(thislearningItem.words.length)];
                        wordReviewItems.push({ singleword: singleWord, item: thislearningItem });
                    }
                }
            }
        }
        return wordReviewItems;
        function getRandomIndex(length) {
            return Math.floor(Math.random() * length);
        }
    }
    private daysBetweenToday(dateMilliseconds) {
        //Get 1 day in milliseconds
        var one_day = 1000 * 60 * 60 * 24;
        // Convert both dates to milliseconds
        var date1_ms = Date.now();
        var date2_ms = new Date(dateMilliseconds).getTime();
        // Calculate the difference in milliseconds
        var difference_ms = date1_ms - date2_ms;
        // Convert back to days and return
        return Math.round(difference_ms / one_day);
    }

    public forceRefreshData() {
        let that = this;
        return that.initializeAllData()
            .then(() => {
                return that.updateReviewItemsData();
            }).then(() => {
                that.initiateDefaultData();
                return;
            })
    }

    public getAdviceLearningItems() {
        var that = this;
        let today = new Date(Date.now());
        let todayDate = today.toISOString().substring(0, 10);
        if (!this.oldestDateOfAdviceLearningItem) {
            this.oldestDateOfAdviceLearningItem = todayDate;
        }
        return new Promise(function (fulfill, reject) {
            if (!hasDataOfDate(todayDate)) {
                that.adviceItem.getThreeAdvicedLearningItem(todayDate).subscribe(
                    data => {
                        if (data) {
                            that.adviceLearningItemsOfAllDate.push(data);
                            that.adviceLearningItemsOfAllDate.sort(function (a, b) {
                                return new Date(b.date).getTime() - new Date(a.date).getTime();
                            })
                            fulfill(abstractPureAdviceLearningItem(that.adviceLearningItemsOfAllDate));
                        }
                    },
                    err => { },
                    () => { }
                );
            } else if (!hasDataOfDate(that.oldestDateOfAdviceLearningItem)) {
                that.adviceItem.getThreeAdvicedLearningItem(that.oldestDateOfAdviceLearningItem).subscribe(
                    data => {
                        if (data) {
                            if(data.data === "no_more_data"){
                                fulfill("no_more_data");
                            }
                            that.adviceLearningItemsOfAllDate.push(data);
                            fulfill(abstractPureAdviceLearningItem(that.adviceLearningItemsOfAllDate));
                        }
                    },
                    err => { },
                    () => { }
                );
            } else {
                fulfill(abstractPureAdviceLearningItem(that.adviceLearningItemsOfAllDate));
            }
        });

        function hasDataOfDate(date) {
            return that.adviceLearningItemsOfAllDate.some((each) => {
                return each.date === date;
            });
        }

        function abstractPureAdviceLearningItem(adviceDataOfAllDate) {
            let resultObject = {};
            adviceDataOfAllDate.forEach((each) => {
                let channels = Object.keys(each.data);
                channels.forEach(eachChannel => {
                    let thisChannelExsitingData = resultObject[eachChannel] || [];
                    thisChannelExsitingData = thisChannelExsitingData.concat(each.data[eachChannel]);
                    resultObject[eachChannel] = thisChannelExsitingData;
                });
            });
            return resultObject;
        }
    }
    public setOldestDateOfAdviceLearningItem() {
        let temp = this.ipc.getneedMoreTimes();
        this.oldestDateOfAdviceLearningItem = this.getDateStringNDaysBefore(temp);
    }
    private getDateStringNDaysBefore(n) {
        let targetDate = new Date(Date.now());
        targetDate.setDate(targetDate.getDate() - n);
        return targetDate.toISOString().substring(0, 10);
    }

    private initiateDefaultData() {
        let that = this;
        if (this.getAllLearningItems().length === 0 && this.getAllTagItems().length === 1) {
            let isChinese = this.ipc.getIsChinese();
            let defaultTags = [
                {
                    _id: "tutalrial1",
                    name: isChinese ? "向左滑动第二项改名" : "Slide Left, Second one to Rename",
                    parent: 'root'
                },
                {
                    _id: 'content',
                    name: isChinese ? "内容相关" : "Content",
                    parent: 'root'
                },
                {
                    _id: 'theme',
                    name: isChinese ? "表达的意思" : "Theme",
                    parent: 'root'
                },
                {
                    _id: 'source',
                    name: isChinese ? "来源" : "Source",
                    parent: 'root'
                },
                {
                    _id: "tutalrial2",
                    name: isChinese ? "向左滑动最后一项添加'子标签'" : "Slide Left, Last one to Add Child",
                    parent: 'source'
                }
            ];
            defaultTags.forEach(tag => {
                that.saveTagItem(tag);
            });
        }
    }
}