import {Injectable} from '@angular/core';
@Injectable()
export class IPCService {
    private transferObject;
    private eventString;
    private isChinese;
    private needToReviewNumber;
    private lastChannel;
    private needMoreTimes = 0;
    constructor() {
    }
    public setTransferObject(object){
        this.transferObject = object;
    }
    // Use getTransferObject and clearTransferObject together.
    public getTransferObject(){
        return this.transferObject;
    }
    public clearTransferObject(){
        this.transferObject = null;
    }
    public publishEvent(event){
        this.eventString = event;
    }
    public getEvent(){
        return this.eventString;
    }
    public clearEvent(){
        this.eventString = null;
    }
    public setIsChinese(isChinese){
        this.isChinese = isChinese;
    }
    public getIsChinese(){
        return this.isChinese;
    }
    public setNeedToReviewNumber(number){
        this.needToReviewNumber = number;
    }
    public getNeedToReviewNumber(){
        return this.needToReviewNumber;
    }
    public setLastChannel(lastChannel){
        this.lastChannel = lastChannel;
    }
    public getLastChannel(){
        return this.lastChannel; 
    }
     public needMoreAdviceLeanringItem(){
        this.needMoreTimes++;
    }
    public getneedMoreTimes(){
        return this.needMoreTimes; 
    }
}